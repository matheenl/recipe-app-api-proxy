# Recipe App API proxy

NGIX proxy app for our recipe app API

## Usage

## Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to which the proxy server forward reqeusts to (default: `app`)
* `APP_PORT` - Port of the app to which the proxy server forward requests to (default: `9000`)